/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.ReceiptDao;
import com.mycompany.dcoffee.model.Receipt;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class TestReceiptDao {

    public static void main(String[] args) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
        String dateFormat = "15/09/2022";
        String timeFormat = "15:35:00";
        Date date = df.parse(dateFormat);
        Date time = tf.parse(timeFormat);
        Receipt receipt = new Receipt(80,1,100,20,12,date,time);
        System.out.println(receipt);
        ReceiptDao receiptDao = new ReceiptDao();
//        receiptDao.save(receipt);
        //Test Get
//        Receipt updateReceipt = receiptDao.get(8);
//        System.out.println(updateReceipt);
//        //Test Upadate
//        updateReceipt.setTotal(2000);
//        receiptDao.update(updateReceipt);
         // Test delete
        receiptDao.delete(receiptDao.get(8));
    }
}
