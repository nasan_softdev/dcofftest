/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.poc;


import com.mycompany.dcoffee.dao.BillDao;
import com.mycompany.dcoffee.model.Bill;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Acer
 */
public class TestBillDao {
    public static void main(String[] args) throws ParseException {
        
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormat = "1/09/2022";
        Date date = df.parse(dateFormat);
        Bill bill = new Bill(date);
        BillDao billDao = new BillDao();

        // Test save
        billDao.save(bill);
        
        // Test update
        Bill updateBill = billDao.get(2);
        String dateFormatUpdate = "9/09/2022";
        Date updatedate = df.parse(dateFormatUpdate);
        updateBill.setDate(updatedate);
        billDao.update(updateBill);

        // Test delete
        billDao.delete(billDao.get(2));
    }
}
