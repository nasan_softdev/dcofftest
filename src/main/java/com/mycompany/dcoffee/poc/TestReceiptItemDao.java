/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffee.poc;

import com.mycompany.dcoffee.dao.ReceiptItemDao;
import com.mycompany.dcoffee.model.ReceiptItem;
import com.mycompany.dcoffee.helper.DatabaseHelper;

/**
 *
 * @author Melon
 */
public class TestReceiptItemDao {
    public static void main(String[] args) {
        ReceiptItem receiptItem = new ReceiptItem(2,40,80);
        ReceiptItemDao  receiptitemDao = new ReceiptItemDao();
        receiptitemDao.save(receiptItem);
    }   
}
