/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dcoffee.dao;

import com.mycompany.dcoffee.helper.DatabaseHelper;
import com.mycompany.dcoffee.model.BillItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class BillItemDao implements Dao<BillItem>{

    public static BillItem get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BillItem get(int id) {
        BillItem billitem = null;
        String sql = "SELECT * FROM bill_item WHERE bill_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billitem = BillItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billitem;
    }

    @Override
    public List<BillItem> getAll() {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM bill_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billitem = BillItem.fromRS(rs);
                list.add(billitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillItem save(BillItem obj) {
        String sql = "INSERT INTO bill_item (bill_item_quantity,bill_item_price_per_unit,bill_item_total_price)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQuantity());
            stmt.setDouble(2, obj.getPriceperunit());
            stmt.setDouble(3, obj.getTotalPrice());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillItem update(BillItem obj) {
        String sql = "UPDATE bill_item"
                + " SET bill_item_quantity = ?, bill_item_price_per_unit = ?, bill_item_total_price = ?"
                + " WHERE bill_item_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQuantity());
            stmt.setDouble(2, obj.getPriceperunit());
            stmt.setDouble(3, obj.getTotalPrice());
            stmt.setInt(4, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillItem obj) {
        String sql = "DELETE FROM bill_item WHERE bill_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<BillItem> getAll(String where, String order) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM bill_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billitem = BillItem.fromRS(rs);
                list.add(billitem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
